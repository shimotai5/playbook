# Ansible
## Errors

> to use the 'ssh' connection type with passwords, you must install the sshpass program

**sshpassをインストールする**
`brew install https://raw.github.com/eugeneoden/homebrew/eca9de1/Library/Formula/sshpass.rb`

> Using a SSH password instead of a key is not possible because Host Key checking is enabled and sshpass does not support this.  Please add this host's fingerprint to your known_hosts file to manage this host.

**sshで1回ログインする**
sshで1回ログインして、known_hostsに追加
`ssh root@192.168.1.105`

## Note
-   [CentOS7 「MySQLインストール・設定」](http://vicsfactory.com/?p=52)